from lesions import *
from optparse import OptionParser
import os, sys


class Top:
    def __init__(self, filename_top, filename_struct, lesion, res):
        self.home_script = sys.path[0] + '/'
        if isinstance(res, list):
            self.res = res  # list of residues to modify
        else:
            self.res = [res]  # one-element list
        self.fname = filename_top
        self.contents = open(self.fname).readlines()
        self.sections = {'h': 'header', 'm': 'moleculetype', 't': 'atoms', 'b': 'bonds', 'p': 'pairs',
                         'a': 'angles', 'd': 'dihedrals', 'i': 'dihedrals', 'f': 'footer'}
        self.dict = {section: self.get_section(section) for section in self.sections.keys()}
        self.ter = self.dict['t'][-1].split()[2:5]  # needed for insertion of dummies by Pdb()
        avail_lesions = {'8oxoG': OxoG(), '8oxo': OxoG(), 'FapyG': FapyG(),
                         'Sp': Sp(), 'O8oxoG': OoxoG(), 'O8oxo': OoxoG()}
        self.lesion = avail_lesions[lesion]
        self.pdb = Pdb(filename_struct, self.lesion, self.res, self.ter)
        self.params = {'b': 1, 'p': 1, 'a': 1, 'd': 9, 'i': 4}
        # TODO instead of at the end, append dummies at the end of the residue & renumber as in itp_merger
        # TODO duplicate lesions.py to contain params for non-alchemical transformations
        # TODO mod interface st two-residue lesions can be processed

    def add_state_b(self):
        atoms = self.dict['t']
        chgs = self.read_charges()
        fstring = '{:>7s}{:>11.4f}{:>11.3f}'
        for r in self.res:
            for l in range(len(atoms)):
                line = atoms[l]
                if len(line.split()) > 7 and line.split()[3] == "DG" and int(line.split()[2]) == r \
                        and not line.startswith(';'):
                    name = line.split()[4]
                    atype = line.split()[1]
                    mass = float(line.split()[7])
                    charge = float(chgs[name])
                    split = line.find(';')-1
                    state_a_end = len(line[:split].rstrip())
                    atoms[l] = line[:state_a_end] + fstring.format(atype, charge, mass) + line[state_a_end:]
        self.dict['t'] = atoms

    def read_charges(self):
        charges_file = self.home_script + self.lesion.name + '.cgs'
        charges = {}
        for line in open(charges_file).readlines():
            lspl = line.strip().split()
            charges[lspl[0]] = lspl[1]
        return charges

    def modify_types(self):
        atoms = self.dict['t']
        to_mod = self.lesion.get_alttypes()
        fstring = "{:>6d}{:>11s}{:>7d}{:>7s}{:>7s}{:>7d}{:>11.4f}{:>11.3f}{:>7s}{:>11.4f}{:>11.3f}"
        for r in self.res:
            for l in range(len(atoms)):
                line = atoms[l]
                if (len(line.split()) > 7 and line.split()[3] == "DG" and line.split()[4] in to_mod.keys()
                        and int(line.split()[2]) == r and not line.startswith(';')):
                    name = line.split()[4]
                    split = line.find(';') - 1
                    state_end = len(line[:split].rstrip())
                    splitline = line.split()
                    for i in [0, 2, 5]:
                        splitline[i] = int(splitline[i])
                    for i in [6, 7, 9, 10]:
                        splitline[i] = float(splitline[i])
                    splitline[8] = to_mod[name][0]
                    if to_mod[name][1] is not None:  # only mod type
                        splitline[10] = to_mod[name][1]
                    atoms[l] = fstring.format(*splitline) + line[state_end:]
        self.dict['t'] = atoms

    def add_dummies(self):
        atoms = self.dict['t']
        fstring = "{:>6d}{:>11s}{:>7d}{:>7s}{:>7s}{:>7d}{:>11.4f}{:>11.3f}{:>7s}{:>11.4f}{:>11.3f}\n"
        for r in self.res:
            for d in self.lesion.get_dummies():
                num = int(atoms[-1].split()[0]) + 1
                atoms.append(fstring.format(num, d[0], r, "DG", d[1], num, *d[2:]))
        self.dict['t'] = atoms

    def modify_param(self, ptype):
        section = self.dict[ptype]
        mods = self.lesion.get_mod_param(ptype)
        for r in self.res:
            for m in mods.keys():
                line = self.find_line(ptype, m, r)
                if line is not None:
                    if ptype in 'di' and len(mods[m]) > 6:  # separate handling of multiple dihedrals
                        assert len(mods[m]) % 6 == 0
                        base = section.pop(line).rstrip()
                        for q in range(int(len(mods[m])/6)):
                            text = base + "".join("{:>10}".format(a) for a in mods[m][6*q:6*(q+1)]) + '\n'
                            section.insert(line+q, text)
                    else:
                        section[line] = section[line].rstrip() + "".join("{:>10}".format(a) for a in mods[m]) + '\n'
                else:
                    print(m, mods[m])
        self.dict[ptype] = section

    def find_line(self, ptype, atoms, res):
        names, nums = self.get_nums(res)
        q = len(atoms)
        section = self.dict[ptype]
        for l in range(len(section)):
            if not section[l][0] in "[;" and all(int(section[l].split()[a]) in names.keys() for a in range(q)):
                if (all([names[int(section[l].split()[a])] == atoms[a] for a in range(q)])
                        or all([names[int(section[l].split()[a])] == atoms[q - a - 1] for a in range(q)])):
                    return l
        print('atoms {} not found'.format(tuple(nums[a] for a in atoms)))

    def add_param(self, ptype):
        section = self.dict[ptype]
        news = self.lesion.get_new_param(ptype)
        for r in self.res:
            names, nums = self.get_nums(r)
            for n in news.keys():
                if ptype in 'di' and len(news[n]) > 6:  # separate handling of multiple dihs
                    assert len(news[n]) % 6 == 0
                    for q in range(int(len(news[n]) / 6)):
                        text = ' '.join('{:5d}'.format(nums[a]) for a in n) + '{:6d}'.format(self.params[ptype]) \
                                + "".join("{:>10}".format(a) for a in news[n][6*q:6*(q+1)]) + '\n'
                        section.append(text)
                else:
                    text = ' '.join('{:5d}'.format(nums[a]) for a in n) + '{:6d}'.format(self.params[ptype])\
                            + "".join("{:>10}".format(a) for a in news[n]) + '\n'
                    section.append(text)
        self.dict[ptype] = section

    def get_bos(self, ptype):  # finds line num that starts respective section
        lines = [i for i in range(len(self.contents)) if len(self.contents[i].split()) > 2
                 and self.contents[i].split()[1] == self.sections[ptype]]
        if len(lines) == 0:
            raise RuntimeError("Section {} not found in the input topology file".format(self.sections[ptype]))
        if ptype in 'mtbpad':
            return lines[0]
        elif ptype == 'i':
            return lines[1]

    def get_section(self, ptype):
        order = {'m': 't', 't': 'b', 'b': 'p', 'p': 'a', 'a': 'd', 'd': 'i'}
        if ptype in 'mtbpad':
            return [l for l in self.contents[self.get_bos(ptype):self.get_bos(order[ptype])] if not l.isspace()]
        elif ptype == 'i':
            return [line for line in self.contents[self.get_bos(ptype):] if line.startswith('[ dih')
                    or line.startswith(';  ai') or (len(line.split()) > 4 and line.split()[4] == '4')]
        elif ptype == 'h':
            return self.contents[:self.get_bos('m')]
        elif ptype == 'f':
            term_line = self.contents.index(self.get_section('i')[-1]) + 1
            return self.contents[term_line:]

    def save_mod(self):
        path = self.fname.split('/')
        path[-1] = self.lesion.name + '-' + path[-1]
        outname = '/'.join(path)
        with open(outname, 'w') as outfile:
            for i in 'hmtbpadif':
                for line in self.dict[i]:
                    outfile.write(line)
                outfile.write('\n')

    def get_nums(self, res):  # one call per residue
        atoms = ("N7", "H7", "C8", "N1", "H1", "C2", "N3", "N9", "C4", "C5", "O5", "H3",
                 "C6", "O6", "H8", "N2", "O4'", "C1'", "C2'", "H1'", "O8", "H9")
        names = {}
        nums = {}
        for i in atoms:
            cont = [int(line.split()[0]) for line in self.dict['t'] if len(line.split()) > 7
                    and line.split()[3] == "DG" and line.split()[4] == i and int(line.split()[2]) == res]
            if len(cont) > 0:
                names[cont[0]] = i
                nums[i] = cont[0]
        return names, nums

    def process_top(self):
        self.add_state_b()
        self.add_dummies()
        self.modify_types()
        for ptype in 'bpadi':
            self.add_param(ptype)
        for ptype in 'badi':
            self.modify_param(ptype)


class Pdb:
    def __init__(self, filename, lesion, res, ter):
        if isinstance(res, list):
            self.res = res  # list of residues to modify
        else:
            self.res = [res]  # one-element list
        self.fname = filename
        self.contents = open(self.fname).readlines()
        self.lesion = lesion
        self.ter = ter  # [resnr, resname, atomname] of original chain terminus

    def add_atoms(self):
        n_added = 0
        for r in self.res:
            atoms = self.lesion.get_new_atoms()
            for a in atoms.keys():
                hook = atoms[a][0][0]
                backing = atoms[a][0][1]
                scale = atoms[a][1]
                h, b = self.find_line(hook, r), self.find_line(backing, r)
                newline = self.gen_line(a, h, b, scale)
                ter = self.find_line(self.ter[2], int(self.ter[0]), self.ter[1])
                self.contents.insert(ter + 1 + n_added, newline)
                n_added += 1

    def find_line(self, name, res, resname="DG"):
        lines = [x for x in range(len(self.contents)) if self.contents[x].startswith("ATOM")
                 and int(self.contents[x][22:26]) == res and self.contents[x][12:16].strip() == name
                 and self.contents[x][17:20].strip() == resname]
        if len(lines) > 1:
            raise RuntimeError("found more than one line fulfilling criterion:\n{}\n"
                               "Consider renumbering DNA residues in your PDB file to avoid duplicates"
                               .format(" ".join([self.contents[a] for a in lines])))
        elif len(lines) == 0:
            raise RuntimeError("name {} and resnum {} not found".format(name, res))
        return lines[0]

    def gen_line(self, name, hook, backing, scale):
        hxyz = self.extract_coords(hook)
        hline = self.contents[hook]
        bxyz = self.extract_coords(backing)
        nxyz = [hxyz[a] + scale*(hxyz[a]-bxyz[a]) for a in range(3)]
        new = hline[:6] + '{:>5}'.format(999) + '{:>4}'.format(name) + hline[15:30] + \
            '{:>8.3f}{:>8.3f}{:>8.3f}'.format(*nxyz) + hline[54:]
        return new

    def extract_coords(self, l):
        line = self.contents[l]
        return [float(line[30+8*a:30+8*(a+1)]) for a in range(3)]

    def save_mod(self):
        path = self.fname.split('/')
        path[-1] = self.lesion.name + '-' + path[-1]
        outname = '/'.join(path)
        with open(outname, 'w') as outfile:
            for line in self.contents:
                outfile.write(line)
            outfile.write('\n')


def parse_options():
    parser = OptionParser(usage="%prog -p top_file -f struct_file -l lesion_name -r residues_to_mutate")
    parser.add_option("-p", dest="topfile", action="store", type="string",
                      help="topology file to be modified (preferably .itp)")
    parser.add_option("-f", dest="structfile", action="store", type="string",
                      help="structure file to be modified (.pdb)")
    parser.add_option("-l", "--lesion", dest="lesion", action="store", type="string",
                      help="lesion to be introduced: 8oxoG, O8oxoG, Sp or FapyG")
    parser.add_option("-r", "--res", dest="res", action="store", type="string",
                      help="residue number to be mutated (to pass multiple numbers, use quotation marks;"
                           "if the lesion spans two residues, pass two residue numbers as well)")
    parser.add_option("--noalch", dest="noalch", action="store", type="string",
                      help="produce a non-alchemical topology, i.e. single-state only")
    
    (opts, args) = parser.parse_args()
    opts.res = [int(x) for x in opts.res.split()]
    return opts

if __name__ == "__main__":
    # TODO implement test case:
    # t = Top('test/topol_DNA_chain_C.itp', 'test/box_prot-dna.pdb', s, [165, 166])
    opt = parse_options()
    t = Top(opt.topfile, opt.structfile, opt.lesion, opt.res)
    t.process_top()
    t.save_mod()
    t.pdb.add_atoms()
    t.pdb.save_mod()
