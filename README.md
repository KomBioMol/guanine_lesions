# make_lesion

A Python script to modify Gromacs topologies so that they can be used to run alchemical transfomations

## Getting Started

The script is ready to use on any machine employed with a working Python distribution; there should be no need for installing extra libraries. Run "python make_lesion.py -h" to see available options.

## Simple usage

  The script is currently capable of adding one or more guanine lesions of the same type, choosing from available options:

+ 7,8-dihydro-8-oxoguanine (8oxoG)
+ the product of 8oxoG two-electron oxidation (O8oxoG)
+ 2,6-diamino-4-hydroxy-5-formamidopyrimidine (FapyG)
+ spiroiminodihydantoin (Sp)

  Both topology (.itp/.top) and structure (.pdb) files are processed to insert state B parameters, dummies, additional and modified bonded terms. Output files are prefixed with the lesion name.

## Examples

  To run the script with the test files provided here, you might try:

```
python make_lesion.py -p test/topol_DNA_chain_C.itp -f test/box_prot-dna.pdb -l Sp -r "165 166"
```

  General usage:

```
python make_lesion.py -p top_file -f struct_file -l lesion_name -r residues_to_mutate
```
