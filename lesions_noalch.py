class Lesion:
    def get_dummies(self):
        dummies = [(self.dummies[d][0][0], d, self.dummies[d][1][0], self.dummies[d][2][0],
                    self.dummies[d][0][1], self.dummies[d][1][1], self.dummies[d][2][1]) for d in self.dummies.keys()]
        return dummies
    
    def get_mod_param(self, ptype):
        return self.mod[ptype]
    
    def get_new_param(self, ptype):
        return self.new[ptype]
    
    def get_alttypes(self):
        return self.alttypes
    
    def get_new_atoms(self):
        return self.newatoms


class OxoG(Lesion):
    def __init__(self):
        self.name = '8oxo'
        self.dummies = {"H7": ("H", 0.4077, 1.008)
                        }
        self.newatoms = {"H7": (("N7", "N9"), 0.55)
                         }
        self.alttypes = {"C8": ("C", None),
                         "H8": ("O", 16)
                         }
        self.mod = {'b':
                        {("C8", "N7"): (0.13040, 442667.2)
                         },
                    'a':
                        {("C8", "N9", "C4"): (105.400, 585.760),
                         ("N9", "C8", "N7"): (113.900, 585.760),
                         ("H8", "C8", "N7"): (123.050, 585.760),
                         ("C8", "N7", "C5"): (103.800, 585.760),
                         },
                    'd':
                        {("O4'", "C1'", "N9", "C8"): (
                            219.8, 1.84096, 3, 4.0, 7.23832, 2, 87.5, 3.38904, 1),
                            ("N9", "C8", "N7", "C5"): (180.0, 41.84000, 2),
                            ("H8", "C8", "N7", "C5"): (180.0, 41.84000, 2)
                        },
                    'i':
                        {("C1'", "N9", "C8", "C4"): (180.00, 4.18400, 2)
                         }
                    }
        self.new = {'b':
                        {("N7", "H7"): (0.10100, 363171.2)
                         },
                    'p':
                        {("C6", "H7"): (),
                         ("C4", "H7"): (),
                         ("H8", "H7"): (),
                         ("N9", "H7"): ()
                         },
                    'a':
                        {("H7", "N7", "C8"): (116.800, 418.400),
                         ("H7", "N7", "C5"): (120.000, 418.400)
                         },
                    'd':
                        {("H7", "N7", "C5", "C6"): (),
                         ("H7", "N7", "C5", "C4"): (),
                         ("H7", "N7", "C8", "H8"): (180.0, 41.84000, 2),
                         ("H7", "N7", "C8", "N9"): (180.0, 41.84000, 2)
                         },
                    'i':
                        {("C8", "C5", "N7", "H7"): (180.0, 4.18400, 2)
                         }
                    }


class OoxoG(Lesion):
    def __init__(self):
        self.name = 'O8oxo'
        self.dummies = {}
        self.newatoms = {}
        self.delete = ["H1"]
        self.alttypes = {"C8": ("C", None),
                         "H8": ("O", 16),
                         "N1": ("NC", None)
                         }
        self.mod = {'b':
                        {("N1", "H1"): (0.10100, 363171.2),
                         ("C8", "N7"): (0.13040, 442667.2)
                         },
                    'a':
                        {("C5", "C6", "N1"): (111.300, 585.760),
                         ("C6", "N1", "H1"): (116.800, 418.400),
                         ("H1", "N1", "C2"): (118.000, 418.400),
                         ("N9", "C8", "N7"): (113.900, 585.760),
                         ("H8", "C8", "N7"): (123.050, 418.400),
                         ("C8", "N7", "C5"): (103.800, 585.760),
                         ("C8", "N9", "C4"): (105.400, 585.760),
                         ("N1", "C2", "N3"): (123.300, 585.760)
                         },
                    'd':
                        {("O4'", "C1'", "N9", "C8"): (
                            219.8, 1.84096, 3, 4.0, 7.23832, 2, 87.5, 3.38904, 1),
                            ("H8", "C8", "N7", "C5"): (180.0, 41.84000, 2),
                            ("N9", "C8", "N7", "C5"): (180.0, 41.84000, 2)
                        },
                    'i':
                        {("C6", "C2", "N1", "H1"): (180.00, 0.00000, 2),
                         ("C1'", "N9", "C8", "C4"): (180.00, 4.18400, 2),
                         ("N2", "N1", "C2", "N3"): (180.00, 4.60240, 2)
                         }
                    }
        self.new = {'b':
                        {},
                    'p':
                        {},
                    'a':
                        {},
                    'd':
                        {},
                    'i':
                        {}
                    }


class FapyG(Lesion):
    def __init__(self):
        self.name = 'FapyG'
        self.dummies = {"O8": ("O", -0.59045, 16),
                        "H7": ("H", 0.42192, 1.008),
                        "H9": ("H", 0.35359, 1.008)
                        }
        self.newatoms = {"O8": (("C8", "H8"), -1.5),
                         "H7": (("N7", "N9"), 0.45),
                         "H9": (("N9", "C4"), 0.75)
                         }
        self.alttypes = {"C8": ("C", None),
                         "N7": ("N", None)
                         }
        self.mod = {'b':
                        {("N7", "C5"): (0.13740, 364844.8, 0.13740, 364844.8)
                         },
                    'a':
                        {("C8", "N7", "C5"): (121.900, 418.400, 121.900, 418.400),
                         ("N7", "C5", "C6"): (121.200, 585.760, 121.200, 585.760),
                         ("N7", "C5", "C4"): (121.200, 585.760, 121.200, 585.760)
                         },
                    'd':
                        {("C8", "N7", "C5", "C6"): (180.0, 0.0, 2, 180.0, 1.2, 2, 0.0, 0.00, 1, 0.0, -27.4, 1),
                            ("C8", "N7", "C5", "C4"): (180.0, 0.0, 2, 180.0, 1.2, 2)
                        },
                    'i':
                        {}
                    }
        self.old = {'b':
                        {("N9", "C8")},
                    'p':
                        # TODO some pairs need to be deleted
                        {},
                    'a':
                        {("C1'", "N9", "C8"),
                         ("C8", "N9", "C4"),
                         ("N9", "C8", "H8"),
                         ("N9", "C8", "N7")
                         },
                    'd':
                        {("O4'", "C1'", "N9", "C8"),
                            ("H1'", "C1'", "N9", "C8"),
                            ("C2'", "C1'", "N9", "C8"),
                            ("C1'", "N9", "C8", "H8"),
                            ("C1'", "N9", "C8", "N7"),
                            ("C4", "N9", "C8", "H8"),
                            ("C4", "N9", "C8", "N7"),
                            ("C8", "N9", "C4", "C5"),
                            ("C8", "N9", "C4", "N3"),
                            ("N9", "C8", "N7", "C5"),
                            ("H7", "N7", "C8", "N9")
                        },
                    'i':
                        {("N9", "N7", "C8", "H8"),
                         ("C1'", "N9", "C8", "C4")
                         }
                    }
        self.new = {'b':
                        {("N7", "H7"): (0.10100, 363171.2),
                         ("N9", "H9"): (0.10100, 363171.2),
                         ("C8", "O8"): (0.12290, 476976.0)
                         },
                    'p':
                        {("C6", "H7"): (),
                         ("C4", "H7"): (),
                         ("H8", "H7"): (),
                         ("O8", "H7"): (),
                         ("N3", "H9"): (),
                         ("C5", "H9"): (),
                         ("H1'", "H9"): (),
                         ("O4'", "H9"): (),
                         ("C2'", "H9"): (),
                         ("H7", "O8"): (),
                         ("C5", "O8"): ()
                         },
                    'a':
                        {("H7", "N7", "C8"): (116.800, 418.400),
                         ("H7", "N7", "C5"): (120.000, 418.400),
                         ("H9", "N9", "C1'"): (118.040, 418.400),
                         ("H9", "N9", "C4"): (125.800, 418.400),
                         ("O8", "C8", "H8"): (119.000, 418.400),
                         ("O8", "C8", "N7"): (122.900, 669.440)
                         },
                    'd':
                        {("H7", "N7", "C8", "H8"): (),
                         ("H7", "N7", "C8", "N9"): (),
                         ("H9", "N9", "C1'", "H1'"): (),
                         ("H9", "N9", "C1'", "C2'"): (),
                         ("H9", "N9", "C1'", "O4'"): (),
                         ("H9", "N9", "C4", "N3"): (),
                         ("H9", "N9", "C4", "C5"): (),
                         ("H7", "N7", "C8", "O8"): (180.0, 10.46000, 2),
                         ("O8", "C8", "N7", "C5"): (180.0, 10.46000, 2, 0.0, 14.50000, 1)
                         },
                    'i':
                        {("C8", "C5", "N7", "H7"): (180.00, 4.60240, 2),
                         ("H8", "O8", "C8", "N7"): (180.00, 4.60240, 2),
                         ("C1'", "C4", "N9", "H9"): (180.00, 4.18400, 2)
                         }
                    }
